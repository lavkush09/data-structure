class Graph {
  constructor() {
    this.numberOfNodes = 0;
    this.adjacencyList = {};
  }

  addNode(node) {
    this.adjacencyList[node] = [];
  }

  nodeConnection(node1, node2) {
    this.adjacencyList[node1].push(node2);
    this.adjacencyList[node2].push(node1);
  }

  showConnections() {
    for (let key in this.adjacencyList) {
      let connections = "";
      let vertex;
      for (vertex of this.adjacencyList[key]) {
        connections += vertex + " ";
      }
      console.log(key, "--->", connections);
    }
  }
}

let newGraph = new Graph();
newGraph.addNode(5);
newGraph.addNode(15);
newGraph.addNode(25);
newGraph.addNode(35);
newGraph.nodeConnection(5,15)
newGraph.nodeConnection(5,25)

newGraph.showConnections();
