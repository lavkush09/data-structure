class stackWithArray {
  constructor() {
    this.data = [];
  }

  push(value) {
    this.data.push(value);
    return this.data;
  }
  pop() {
    this.data.pop();
    return this.data;
  }

  peek() {
    return this.data[this.data.length - 1] || [];
  }
}
module.exports = stackWithArray;
// export stackWithArray;

// let newStack = new stackWithArray();
// console.log(newStack.peek());
// console.log(newStack.push(5));
// console.log(newStack.push(15));
// console.log(newStack.peek());
// console.log(newStack.pop());
