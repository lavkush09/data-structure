class MyArray {
  constructor() {
    this.length = 0;
    this.data = {};
  }

  /* time complexity is O(i) */
  get(index) {
    return this.data[index];
  }

  /* time complexity is O(1) */
  add(item) {
    this.data[this.length] = item;
    this.length++;
    return this.data;
  }

  /* time complexity is O(1) */
  remove() {
    const tempData = this.data[this.length - 1];
    delete this.data[this.length - 1];
    this.length--;
    return tempData;
  }

  /* time complexity is O(n) */
  deleteByIndex(index) {
    if (index > this.length - 1) {
      throw "array out of index";
    }
    let item = this.data[index];
    this.shiftElement(index);
    return item;
  }

  /* time complexity is O(n) */
  insertByIndex(index, item) {
    this.shiftingAndInitialization(index, item);
    return this.data;
  }

  /* helper function for insertByIndex */
  shiftingAndInitialization(index, item) {
    let temObj = { ...this.data };
    let actualIndex = index;
    if (index < 0) {
      actualIndex = 0;
    } else if (index > this.length - 1) {
      actualIndex = this.length;
    } else {
      actualIndex = index;
    }
    for (let i = actualIndex; i < this.length; i++) {
      this.data[i + 1] = temObj[i];
    }
    this.data[actualIndex] = item;
    this.length++;
  }

  /* helper function for deleteByIndex */
  shiftElement(index) {
    for (let i = index; i < this.length - 1; i++) {
      this.data[i] = this.data[i + 1];
    }
    delete this.data[this.length - 1];
    this.length--;
  }
}

const newMyArray = new MyArray();
newMyArray.add("hi");
newMyArray.add("hello");
newMyArray.add("this is new");
newMyArray.remove();
newMyArray.insertByIndex(5, "index incretion");
newMyArray.insertByIndex(2, "index2 incretion");
newMyArray.deleteByIndex(0);
console.log(newMyArray.get(0));
