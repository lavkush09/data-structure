class HashTable {
  constructor(num) {
    this.data = new Array(num);
  }

  _hashing(key) {
    let hash = 0;
    for (let i = 0; i < key.length; i++) {
      hash = (hash + key.charCodeAt(i) * i) % key.length;
    }
    return hash;
  }

  set(key, value) {
    let memoryAddress = this._hashing(key);
    if (!this.data[memoryAddress]) {
      this.data[memoryAddress] = [];
    }
    // this.checkKey()
    this.data[memoryAddress].push([key, value]);
    return this.data;
  }

  get(key) {
    let memoryAddress = this._hashing(key);
    let currentBucket = this.data[memoryAddress];
    if (currentBucket) {
      for (let i = 0; i < currentBucket.length; i++) {
        if (currentBucket[i][0] === key) {
          return currentBucket[i][1];
        }
      }
      return undefined;
    }
  }

  keys() {
    let key = [];
    for (let i = 0; i < this.data.length; i++) {
      if (this.data[i]) {
        for (let j = 0; j < this.data[i].length; j++) {
          key.push(this.data[i][j][0]);
        }
      }
    }
    return key;
  }
}

let hs = new HashTable(500);
hs.set("key1", 100);
hs.set("key1", 100);
hs.set("key7", 1200);
let res = hs.set("key3", 1400);
console.log(hs.keys());
