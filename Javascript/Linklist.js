class NewNode {
  constructor(value) {
    this.value = value;
    this.next = null;
  }
}

class LinkList {
  constructor(value) {
    this.head = new NewNode(value);
    this.tail = this.head;
    this.length = 1;
  }

  append(value) {
    let newNode = new NewNode(value);
    this.tail.next = newNode;
    this.tail = newNode;
    this.length++;
    return this;
  }

  printList() {
    let currentNode = this.head;
    let array = [];
    while (currentNode !== null) {
      array.push(currentNode.value);
      currentNode = currentNode.next;
    }
    return array;
  }

  prepend(value) {
    let newNode = new NewNode(value);
    newNode.next = this.head;
    this.head = newNode;
    this.length++;
    return this.printList();
  }

  insert(index, value) {
    if (this.length < index) {
      return this.append(value);
    } else if (index <= 0) {
      return this.prepend(value);
    }
    let newNode = new NewNode(value);
    let leadNode = this.traverseNode(index - 1);
    newNode.next = leadNode.next;
    leadNode.next = newNode;
    this.length++;
    return this.printList();
  }

  remove(index) {
    if (index > this.length || index < 0) {
      throw "out of index";
    } else if (index == 0) {
      let headNoe = this.head.next;
      this.head = headNoe;
      return this.printList();
    }
    let leadNode = this.traverseNode(index - 1);
    let unWantedNode = leadNode.next;
    leadNode.next = unWantedNode.next;
    this.length--;
    return this.printList();
  }

  traverseNode(index) {
    let counter = 0;
    let currentNode = this.head;
    while (counter < index) {
      currentNode = currentNode.next;
      counter++;
    }
    return currentNode;
  }
}

let newList = new LinkList(9);
newList.append(7);
newList.append(17);
newList.append(27);
newList.prepend(910);
newList.insert(0, 99);
console.log(newList.printList());
console.log(newList.remove(3));
