class Node {
  constructor(value) {
    this.value = value;
    this.next = null;
  }
}

class Stack {
  constructor() {
    this.top = null;
    this.bottom = null;
    this.length = 0;
  }

  peek() {
    return this.top;
  }

  push(value) {
    let newNode = new Node(value);
    if (this.length === 0) {
      this.top = newNode;
      this.bottom = newNode;
    } else {
      let currentNode = this.top;
      this.top = newNode;
      this.top.next = currentNode;
    }
    this.length++;
    return this;
  }

  pop() {
    if (!this.top) {
      return this;
    }
    if (this.top === this.bottom) {
      this.bottom = null;
    }
    let currentTop = this.top;
    this.top = currentTop.next;
    this.length--;
    return this;
  }
}

module.exports = Stack;

newStack = new Stack();
newStack.push(115);
newStack.push(25);
console.log(newStack.pop());
console.log(newStack.push(15));
console.log(newStack.pop());
console.log(newStack.peek());
