class Node {
  constructor(value) {
    this.value = value;
    this.next = null;
    this.prev = null;
  }
}

class DoublyLinkList {
  constructor(value) {
    this.head = new Node(value);
    this.tail = this.head;
    this.length = 1;
  }

  append(value) {
    let newNode = new Node(value);
    newNode.prev = this.tail;
    this.tail.next = newNode;
    this.tail = newNode;
    this.length++;
    return this.printList();
  }

  prepend(value) {
    let newNode = new Node(value);
    newNode.next = this.head;
    this.head = newNode;
    this.length++;
    return this.printList();
  }

  insert(index, value) {
    if (this.length < index) {
      return this.append(value);
    } else if (index <= 0) {
      return this.prepend(value);
    }
    let newNode = new Node(value);
    let leaderNode = this.traverseNode(index - 1);
    let secondNeboNode = leaderNode.next;
    newNode.next = secondNeboNode;
    secondNeboNode.prev = newNode;
    newNode.prev = leaderNode;
    leaderNode.next = newNode;
    this.length++;
    return this.printList();
    // return this.head;
  }
  printList() {
    let listOfArray = [];
    let currentNode = this.head;
    while (currentNode !== null) {
      listOfArray.push(currentNode.value);
      currentNode = currentNode.next;
    }
    return listOfArray;
  }

  traverseNode(index) {
    let count = 0;
    let currentNode = this.head;
    while (count !== index) {
      currentNode = currentNode.next;
      count++;
    }
    return currentNode;
  }

  remove(index) {
    if (index > this.length || index < 0) {
      throw "out of index";
    } else if (index == 0) {
      let headNoe = this.head.next;
      headNoe.prev = null;
      this.head = headNoe;
      return this.printList();
    }
    let leaderNode = this.traverseNode(index - 1);
    let unwantedNode = leaderNode.next;
    let preNode = unwantedNode.next;
    preNode.prev = leaderNode;
    leaderNode.next = preNode;
    this.length--;
    return this.printList();
  }
}

let DList = new DoublyLinkList(5);
console.log(DList.append(72));
console.log(DList.prepend(1112));
console.log(DList.insert(2, 18112));
// console.log(DList.remove(10));
