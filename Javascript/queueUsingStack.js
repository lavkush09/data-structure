const StackWithArray = require("./stackWithArray");
// const Stack = require("./stack");

class QueueWithStack {
  constructor() {
    this.stack1 = new StackWithArray();
    this.stack2 = new StackWithArray();
  }

  enqueue(value) {
    this.stack1.push(value);
    return this.stack1.data;
  }

  dequeue() {
    this.stackController(this.stack1, this.stack2);
    this.stack2.pop();
    this.stackController(this.stack2, this.stack1);
    return this.stack1;
  }

  peek() {
    this.stackController(this.stack1, this.stack2);
    let firstEle = this.stack2.peek();
    this.stackController(this.stack2, this.stack1);
    return firstEle;
  }

  stackController(mainStack, secondStack) {
    let stackLength = mainStack.data.length ;
    for (let i = 0; i < stackLength; i++) {
      secondStack.push(mainStack.peek());
      mainStack.pop();
    }
  }
}

let newQueue = new QueueWithStack();
newQueue.enqueue(5);
newQueue.enqueue(15);
newQueue.enqueue(8);
newQueue.enqueue(9);
newQueue.enqueue(78);
newQueue.dequeue();
newQueue.dequeue();
console.log(newQueue.dequeue());
console.log(newQueue.peek());
