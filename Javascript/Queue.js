class Node {
  constructor(value) {
    this.value = value;
    this.next = null;
  }
}

class Queue {
  constructor() {
    this.first = null;
    this.last = null;
    this.length = 0;
  }

  enqueue(value) {
    let newNode = new Node(value);
    if (this.length === 0) {
      this.first = newNode;
      this.last = newNode;
    }
    this.last.next = newNode;
    this.last = newNode;
    this.length++;
    return this;
  }
  dequeue() {
    if (!this.first) {
      return this;
    }
    if (this.first === this.last) {
      this.last = null;
    }
    this.first = this.first.next;
    this.length--;
    return this;
  }
}

let qe = new Queue();

console.log(qe.enqueue(4));
console.log(qe.enqueue(14));
console.log(qe.enqueue(124));
console.log(qe.dequeue());
console.log(qe.dequeue());
console.log(qe.dequeue());
