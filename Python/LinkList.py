import pprint


class LinkList():
    def __init__(self, value):
        self.head = {
            'value': value,
            'nextNode': None
        }
        self.tail = self.head
        self.length = 1

    def append(self, value):
        newNode = {
            'value': value,
            'nextNode': None
        }
        self.tail['nextNode'] = newNode
        self.tail = newNode
        self.length += 1
        return self.print_values_as_list()

    def prepend(self, value):
        newNode = {
            'value': value,
            'nextNode': None
        }
        newNode['nextNode'] = self.head
        self.head = newNode
        self.length += 1
        return self.print_values_as_list()

    def print_values_as_list(self):
        currentNode = self.head
        array_list = list()
        while(currentNode):
            array_list.append(currentNode['value'])
            currentNode = currentNode['nextNode']
        return array_list

    def insert(self, index, value):
        if index <= 0:
            self.prepend(value)
            return self.print_values_as_list()
        elif index > self.length:
            self.append(value)
            return self.print_values_as_list()
        newNode = {
            'value': value,
            'nextNode': None
        }
        parentNode = self.get_parent_node(index-1)
        newNode['nextNode'] = parentNode['nextNode']
        parentNode['nextNode'] = newNode
        self.length += 1
        return self.print_values_as_list()

    def remove(self, index):
        if index < 0 or index > self.length:
            raise Exception('out of index')
        elif index == 0:
            self.head = self.head['nextNode']
            return self.print_values_as_list()
        parentNode = self.get_parent_node(index-1)
        currentNode = parentNode['nextNode']
        parentNode['nextNode'] = currentNode['nextNode']
        self.length -= 1
        return self.print_values_as_list()

    def get_parent_node(self, index):
        currentNode = self.head
        i = 0
        while(i != index):
            currentNode = currentNode['nextNode']
            i += 1
        return currentNode


pp = pprint.PrettyPrinter(indent=4)
new_linkList = LinkList(5)
new_linkList.append(6)
new_linkList.prepend(106)
new_linkList.prepend(16)
new_linkList.insert(-14, 1236)
pp.pprint(new_linkList.remove(0))
