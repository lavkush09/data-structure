import pprint
pp = pprint.PrettyPrinter(indent=4)


class DlinkList():
    def __init__(self, value):
        self.head = {
            "value": value,
            "preNode": None,
            "nextNode": None
        }
        self.tail = self.head
        self.length = 0

    def append(self, value):
        newNode = {
            "value": value,
            "preNode": None,
            "nextNode": None
        }
        newNode['preNode'] = self.tail
        self.tail['nextNode'] = newNode
        self.tail = newNode
        self.length += 1
        return self.print_values_as_list()

    def prepend(self, value):
        newNode = {
            "value": value,
            "preNode": None,
            "nextNode": None
        }
        newNode["nextNode"] = self.head
        self.head['preNode'] = newNode
        self.head = newNode
        self.length += 1
        return self.print_values_as_list()

    def incert(self, index, value):
        if index <= 0:
            self.prepend(value)
            return self.print_values_as_list()
        elif index > self.length:
            self.append(value)
            return self.print_values_as_list()
        newNode = {
            "value": value,
            "preNode": None,
            "nextNode": None
        }
        parentNode = self.get_parent_node(index-1)
        secondNextNode = parentNode['nextNode']
        newNode['nextNode'] = parentNode['nextNode']
        newNode['preNode'] = parentNode
        secondNextNode['preNode'] = newNode
        parentNode['nextNode'] = newNode
        self.length += 1
        return self.print_values_as_list()

    def remove(self, index):
        if index < 0 or index > self.length:
            raise Exception('out of index')
        elif index == 0:
            self.head = self.head['nextNode']
            self.head['preNode'] = None
            return self.print_values_as_list()
        parentNode = self.get_parent_node(index-1)
        currentNode = parentNode['nextNode']
        lastNode = currentNode['nextNode']
        parentNode['nextNode'] = lastNode
        lastNode['preNode'] = parentNode
        self.length -= 1
        return self.print_values_as_list()

    def get_parent_node(self, index):
        currentNode = self.head
        i = 0
        while(i != index):
            currentNode = currentNode['nextNode']
            i += 1
        return currentNode

    def print_values_as_list(self):
        currentNode = self.head
        array_list = list()
        while(currentNode):
            array_list.append(currentNode['value'])
            currentNode = currentNode['nextNode']
        return array_list


newlist = DlinkList(5)
newlist.append(14)
newlist.append(24)
newlist.append(4)
newlist.prepend(10)
newlist.incert(1, 42)
pp.pprint(newlist.incert(0, 420))
pp.pprint(newlist.remove(5))
