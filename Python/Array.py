from copy import copy


class MyArray():
    def __init__(self):
        self.data = {}
        self.length = 0

    """ time complexcity O(1) """

    def get_index(self, index):
        try:
            return self.data[index]
        except Exception as e:
            return "undefined"

    """ time complexcity O(1) """

    def append(self, value):
        self.data[self.length] = value
        self.length += 1

    """ time complexcity O(1) """

    def pop(self):
        del self.data[self.length-1]
        self.length -= 1
        return self.data

    """ time complexcity O(n) """

    def incert_by_index(self, index, value):
        if self.length > index and index >= 0:
            self.shift_operation(index)
            self.data[index] = value
        elif index < 0:
            self.shift_operation(0)
            self.data[0] = value
        else:
            self.append(value)
        return self.data

    def shift_operation(self, index):
        print(index)
        tem_value = copy(self.data)
        for i in range(index, self.length):
            self.data[i+1] = tem_value[i]
        self.length += 1


a = MyArray()
a.append(10)
a.append(20)
a.incert_by_index(-7, 30)
a.incert_by_index(10, 30)
print(a.incert_by_index(3, 450), a.incert_by_index(42, 3560), a.length)
