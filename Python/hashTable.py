class HashTable():
    def __init__(self, num=2):
        self.data = [[] for _ in range(num)]

    def hash_genrater(self, key):
        return hash(key) % len(self.data)

    def add(self, key, value):
        hash_key = self.hash_genrater(key)
        memory_loc = self.data[hash_key]
        if len(memory_loc) > 0:
            for place in memory_loc:
                if place[0] == key:
                    place[1] = value
                    return place
        memory_loc.append([key, value])
        return [key, value]

    def get(self, key):
        hash_key = self.hash_genrater(key)
        value_container = self.data[hash_key]
        for v in value_container:
            if v[0] == key:
                return v[1]
        raise Exception('key not present')

    def keys(self):
        list_of_keys = []
        for li in self.data:
            for key in li:
                list_of_keys.append(key[0])
        return list_of_keys


newHash = HashTable(10)
print(newHash.add('key1', 'value1'))
print(newHash.get('key1'))
print(newHash.add('key1', 100))
newHash.add('key', 'dfghj')
print(newHash.add('key3', 12224))
print(newHash.keys())
